package router

//lint:file-ignore ST1019 we love to import more then once
import (
	"encoding/json"

	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/google/go-github/github"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/ateam/dover/constants"
	gitutil "gitlab.com/ateam/dover/pkg/githubutil"
	"gitlab.com/ateam/dover/pkg/gitlabutil"
)

func ReadDataFromEndPoint(w http.ResponseWriter, r *http.Request) (*os.File, error) {
	handlerLogger := log.New().WithFields(logrus.Fields{"handler": "PackageListHandler"})
	var tempFile *os.File
	var err error

	err = r.ParseMultipartForm(32 << 20) // maxMemory 32MB
	if err != nil {
		handlerLogger.Error("cannot parse multiPartForm", "error", err)
		w.WriteHeader(http.StatusBadRequest)

		return nil, err
	}

	err = os.MkdirAll(constants.PackageListFileDirectory, os.ModePerm)
	if err != nil {
		handlerLogger.Error("cannot mkdir", "error", err)
		w.WriteHeader(http.StatusInternalServerError)

		return nil, err
	}

	fileName := r.Form.Get("fileName")
	tempFile, err = os.Create(constants.PackageListFileDirectory + fileName)
	if err != nil {
		handlerLogger.Error("cannot create file", "error", err)
		w.WriteHeader(http.StatusInternalServerError)

		return nil, err
	}

	// TODO: we should avoid making any changes in data for the MR.
	// "rome" should send the data to dover which should be sent untouched to raise a MR
	data := strings.Replace(r.Form.Get("data"), "'", "", 2)
	receivedData := json.RawMessage(data)
	jsonData, err := json.MarshalIndent(receivedData, "", "    ")

	if err != nil {
		handlerLogger.Error("cannot marshal data to json", "error", err)
		w.WriteHeader(http.StatusInternalServerError)

		return nil, err
	}

	// Add a new line at the end of the data so that the file has a new line as EOF
	// similar to the one in the repository
	jsonData = append(jsonData, "\n"...)

	err = ioutil.WriteFile(tempFile.Name(), []byte(jsonData), 0644)

	if err != nil {
		handlerLogger.Error("cannot write into file", "error", err)
		w.WriteHeader(http.StatusInternalServerError)

		return nil, err
	}

	return tempFile, err
}

// SubmitPackageListHandlerGitlab is the handler for submitting githlab MRs
// TODO: Need to refactor when the gitutil package is refactored to have similar interfaces
func SubmitPackageListHandlerGitlab(w http.ResponseWriter, r *http.Request) {

	tempFile, err := ReadDataFromEndPoint(w, r)

	if err != nil {
		log.Errorf("router.ReadDataFromEndPoint error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {
			return
		}
	}(constants.PackageListFileDirectory)
	defer func(tempFile *os.File) {
		err := tempFile.Close()
		if err != nil {
			return
		}
	}(tempFile)

	var gitlab gitlabutil.GitForge = &gitlabutil.Gitlab{Client: &gitlab.Client{}}
	gitClient := gitlabutil.NewGitClientAttr(&gitlab)

	if gitClient == nil {
		log.Errorf("gitlabutils.NewGitClientAttr error")
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	envVar := gitlabutil.EnvVars{}
	// Read the environment vars before creating the client
	if envVar.GetEnv() != nil {
		log.Errorf("gitlabutils.EnvVars error")
		return
	}

	gitClient.Client.CreateClient(&envVar)

	err = gitClient.SetBaseURLWrapper(constants.GitlabBaseUrl)

	if err != nil {
		log.Errorf("gitlabutils.SetBaseURLWrapper error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	branchAttr := gitlabutil.BranchAttr{SourceBranch: r.Form.Get("branch"), TargetBranch: constants.GitlabProdBranch}
	br, err := gitClient.CreateBranchWrapper(&gitlabutil.RepoAttr{ProjectID: envVar.GitlabProjectID}, &branchAttr)

	if err != nil || br == nil {
		log.Errorf("gitlabutils.CreateBranchWrapper error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	commitMessage := r.Form.Get("commitMessage")
	commit, err := gitClient.CreateCommitWrapper(&gitlabutil.RepoAttr{ProjectID: envVar.GitlabProjectID},
		&gitlabutil.CommitAttr{AuthorName: constants.RomeBotName,
			AuthorEmail: constants.RomeBotEmail, CommitMsg: commitMessage,
			SourceFile: tempFile.Name()}, &branchAttr)
	if err != nil || commit == nil {
		log.Errorf("gitlabutils.CreateCommitWrapper error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	mr, err := gitClient.CreateMRWrapper(&gitlabutil.RepoAttr{ProjectID: envVar.GitlabProjectID},
		&gitlabutil.MergeAttr{Title: "Rome-bot Automated Merge Request Change", Description: commitMessage}, &branchAttr)

	if err != nil || mr == nil {
		log.Errorf("gitlabutils.CreateMRWrapper error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	_ = os.Remove(constants.PackageListFileDirectory + "/" + tempFile.Name())

	w.WriteHeader(http.StatusOK)
}

// SubmitPackageListHandlerGithub is the handler for submitting github PRs
// TODO: Need to refactor when the gitutil package is refactored to have similar interfaces
func SubmitPackageListHandlerGithub(w http.ResponseWriter, r *http.Request) {
	tempFile, err := ReadDataFromEndPoint(w, r)
	if err != nil {
		log.Errorf("router.ReadDataFromEndPoint error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	gitClient, err := gitutil.NewGitClient()
	if err != nil {
		log.Errorf("cannot get new git client, error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	branchName := r.Form.Get("branch")
	commitMessage := r.Form.Get("commitMessage")
	sourceFiles := []*string{github.String(constants.PackageListFileDirectory + tempFile.Name())}

	err = gitClient.SubmitPR(branchName, commitMessage, sourceFiles)
	if err != nil {
		log.Errorf("cannot submit PR, error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	_ = os.Remove(constants.PackageListFileDirectory + tempFile.Name())

	w.WriteHeader(http.StatusOK)
}

// HealthCheckHandler handles the health endpoint.
func HealthCheckHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	_, _ = io.WriteString(w, `{"alive": true}`)
}
