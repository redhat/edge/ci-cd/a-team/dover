package router

import (
	"net/http"

	logger "github.com/sirupsen/logrus"
	"gitlab.com/ateam/dover/constants"

	"github.com/gorilla/mux"
)

// StartServer sets up endpoints, handlers and starts a new HTTP server at 127.0.0.1:8080.
func StartServer() error {
	router := mux.NewRouter()
	router.Path("/submit/package_list").Methods("POST").HandlerFunc(SubmitPackageListHandlerGitlab)
	router.Path("/health").Methods("GET").HandlerFunc(HealthCheckHandler)

	srv := &http.Server{
		Addr:         constants.DoverPort,
		Handler:      router,
		ReadTimeout:  constants.TimeoutRead,
		WriteTimeout: constants.TimeoutWrite,
	}

	logger.Info(":: service started :: ", "address", srv.Addr)

	return srv.ListenAndServe()
}
